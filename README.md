# CleverSheep Releases

This repository contains releases of the CleverSheep testing framework.

For more details see https://lcaraccio.gitlab.io/clever-sheep/api/index.html or email cleversheepframework@gmail.com
